from django.db import models
from django.contrib.auth.models import User
import os
import uuid
# Create your models here.

class Group(models.Model):


    @staticmethod
    def generate_new_filename(instance, filename):
        f, ext = os.path.splitext(filename)
        print '%s%s' % (uuid.uuid4().hex, ext)
        return '%s%s' % (uuid.uuid4().hex, ext)

    name = models.CharField(max_length=40)
    topic = models.TextField()
    paper = models.FileField(upload_to='files/%Y/%m/%d/')

    def __unicode__(self):
        return self.name


class Teacher(models.Model):
    user = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    def __unicode__(self):
        return ("%s %s") % (self.name, self.last_name)


class Student(models.Model):
    user = models.OneToOneField(User)
    group = models.ForeignKey(Group)

    def __unicode__(self):
        return "%s" % self.user.username


class Vote(models.Model):
    group = models.ForeignKey(Group)
    grade = models.IntegerField(max_length=2)
    student = models.ForeignKey(Student)
    comment = models.TextField(null=True, blank=True)


