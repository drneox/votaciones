# Create your views here.
from django.http import Http404
from django.shortcuts import render_to_response
from models import Group, Student, Vote
from django.db.models import Count, Min, Sum, Avg
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout


def group_detail(request, id):
    try:
        group = Group.objects.get(id=id)
        votes = Vote.objects.filter(group=group)
        try:
            student = Student.objects.filter(group=group)
        except Student.DoesNotExist:
            raise Http404
    except Group.DoesNotExist:
        raise Http404
    print request.user.is_active
    return render_to_response('group/show.html', {'group': group, 'votes': votes, 'students': student},
                              context_instance=RequestContext(request))


def group_all(request):
    try:
        group = Group.objects.all()
    except Group.DoesNotExist:
        raise Http404
    return render_to_response('group/index.html', {'groups': group}, context_instance=RequestContext(request))


def student_detail(request, code):
    try:
        student = Student.objects.get(user__username=code)
    except Student.DoesNotExist:
        raise Http404
    return render_to_response('student/show.html', {'student': student})

@login_required(login_url='/login')
def vote(request):
    if request.method == 'POST':
        if request.user:
            grade = int(request.POST.get("grade", ""))
            group = request.POST.get("group", "")
            comment = request.POST.get("comment", "").encode('utf-8')
            get_group = Group.objects.get(id=group)
            try:
                get_student = Student.objects.get(user=request.user)
                            # restringir voto repetido
                try:
                    Vote.objects.get(group=get_group, student=get_student)
                    print "no voto"
                    return redirect('/grupos/%s' % group)
                except Vote.DoesNotExist:
                    # restringir voto fuera del rango
                    print grade
                    if grade <= 20 and grade >= 0:
                        #restringir el voto al mismo grupo
                        if get_student.group != get_group:
                            vote = Vote(group=get_group, grade=grade, student=get_student, comment=comment)
                            vote.save()
                            print "voto"
                    return redirect('/grupos/%s' % group)
            except Student.DoesNotExist:
                return redirect('/login/')
                not_vote = True
        else:
            return redirect('/grupos/')
    else:
        return redirect('/grupos/')


def login_auth(request):
    if request.method == 'POST':
        code = request.POST.get("code", "")
        password = request.POST.get("password", "")
        print code, password
        user = authenticate(username=code, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect("/grupos/")
            else:
                return redirect("/login")
        else:
            return redirect("/login")
    else:
        #print request.user.is_active
        return render_to_response("users/login.html", context_instance=RequestContext(request))


def ranking(request):
    ranking = Vote.objects.all().values('group').annotate(score = Avg('grade')).order_by('-score')

    print ranking
    for score in ranking:
        group_detail = Group.objects.get(id=score['group'])
        score['group_detail'] = group_detail
        print score
    return render_to_response("ranking.html", {'rank': ranking},  context_instance=RequestContext(request))


def index(request):
    return render_to_response("index.html")


def logout_view(request):
    logout(request)
    return redirect('/login/')



