from django.contrib import admin
from models import Student, Group, Vote, Teacher

admin.site.register(Student)
admin.site.register(Group)
admin.site.register(Vote)
admin.site.register(Teacher)