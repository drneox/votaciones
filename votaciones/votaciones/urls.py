from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
import settings
urlpatterns = patterns('',
    # Examples:


     url(r'^alumnos/(\w+)$', 'votos.views.student_detail', name='student_detail'),
     url(r'^grupos/(\d+)$', 'votos.views.group_detail', name='group_detail'),
    # url(r'^votaciones/', include('votaciones.foo.urls')),
     url(r'^grupos/$', 'votos.views.group_all', name='group_all'),
     url(r'^login/$', 'votos.views.login_auth', name='login'),
     url(r'^votos/$', 'votos.views.vote', name='voto'),
     url(r'^ranking/$', 'votos.views.ranking', name='ranking'),
     url(r'^$', 'votos.views.index', name='ranking'),
     url(r'^logout/$', 'votos.views.logout_view', name='logout'),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('',
                            url(r'^static/(?P<path>.*)$',
                                'django.views.static.serve', {
                                    'document_root': settings.MEDIA_ROOT,
                                }),
                            (r'^media/(?P<path>.*)$',
                             'django.views.static.serve', {
                                'document_root': settings.MEDIA_ROOT})
    )